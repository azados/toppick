const { ApolloServer } = require('apollo-server');
const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

// * Import typeDefs and resolvers
const filePath = path.join(__dirname, 'typeDefs.gql');
const typeDefs = fs.readFileSync(filePath, 'utf-8');
const resolvers = require('./resolvers');

// * Import Environment Variables and Mongoose Models
require('dotenv').config({ path: 'variables.env' });
const User = require('./models/User');
const Item = require('./models/Item');

// Connect to mLab Database 
mongoose
  .connect(
    process.env.MONGO_URI, 
    { useNewUrlParser: true }
    )
  .then(() => console.log('mLab MongoDB connected'))
  .catch(err => console.error(err));
  mongoose.set('useCreateIndex', true);

// Create Apollo-server/GraphQL using typeDefs, resolvers, and context object
const server = new ApolloServer({ 
  typeDefs,
  resolvers,
  context: {
    User,
    Item
  }
});

server.listen(5000).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});