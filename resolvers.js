module.exports = {
    Query: {
        getItems: async (_, args, { Item }) => {
            const items = await Item.find({})
            .sort({ createdDate: 'desc' })
            .populate({
                path: 'createdBy',
                model: 'User'
            });
            return items;
        }
    },
    Mutation: {
        addItem: async (
            _, 
            { title, imageUrl, categories, description, creatorId }
            , { Item }
            ) => {
            const newItem = await new Item({ 
                title, 
                imageUrl, 
                categories, 
                description, 
                createdBy: creatorId })
            .save();
            return newItem;
        },
        createUser: async (_, { username, email, password }, { User }) => {
            const user = await User.findOne({ username });
            if (user) {
                throw new Error('User already exist');
            }
            const newUser = await new User({ 
                username, 
                email, 
                password 
            }).save();
            return newUser;
        }
    }
};